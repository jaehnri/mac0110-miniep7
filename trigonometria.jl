# MAC0110 - MiniEP7
# João Henri Carrenho Rocha - 11796378

using Test

const iteration = 10

function taylor_sin(x)
    signal = 1
    sum = 0
    taylor_counter = 1

    for i in 1:iteration
        sum = sum + (x^taylor_counter / factorial(big(taylor_counter))) * signal
        taylor_counter += 2
        signal = signal * (-1)
    end

    return sum
end

function taylor_cos(x)
    signal = -1
    sum = 1
    taylor_counter = 2

    for i in 1:iteration
        sum = sum + (x^taylor_counter / factorial(big(taylor_counter))) * signal
        taylor_counter += 2
        signal = signal * (-1)
    end

    return sum
end

function taylor_tan(x)
    sum = 0

    for n in 1:iteration
        coefficient = BigInt((2^(2 * n)) * ((2^(2 * n)) - 1))
        xn = BigFloat((x^(2 * n - 1)))
                
        sum += (coefficient * bernoulli(n) * xn) / (factorial(2*n))
    end

    return sum
end

function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0:n
		A[m + 1] = 1 // (m + 1)
		for j = m:-1:1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return float(abs(A[1]))
end

function check_taylor_sin()
    @test taylor_sin(0) ≈  0
    @test taylor_sin(pi/3) ≈  sqrt(3)/2 atol = 0.001
    @test taylor_sin(pi/4) ≈  sqrt(2)/2 atol = 0.001
    @test taylor_sin(pi/6) ≈  1/2 atol = 0.001
end

function check_taylor_cos()
    @test taylor_cos(0) ≈  1
    @test taylor_cos(pi/3) ≈  1/2 atol = 0.001
    @test taylor_cos(pi/4) ≈  sqrt(2)/2 atol = 0.001
    @test taylor_cos(pi/6) ≈  sqrt(3)/2 atol = 0.001
end

function check_taylor_tan()
    @test taylor_tan(0) ≈  0
    @test taylor_tan(pi/3) ≈  sqrt(3) atol = 0.001
    @test taylor_tan(pi/4) ≈  1 atol = 0.001
    @test taylor_tan(pi/6) ≈  sqrt(3)/3 atol = 0.001
end

function test()
    check_taylor_sin()
    check_taylor_cos()
    check_taylor_tan()
end